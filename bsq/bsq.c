int				**do_numbers(char **map_s, t_map *map)
{
	int map_n[map->x][map->y];
	int x;
	int y;

	x = 0;
	y = 0;
	while (x <= map->x)
	{
		while (y <= map->y)
		{
			if (map_s[x][y] == map->empty)
				map_n[x][y] = 1;
			if (map_s[x][y] == map->obs)
				map_n[x][y] = 0;
			y++;
		}
		x++;
	}
	return (map_n);
}

t_square		**do_good_numbers(int **map_n, t_map *map)
{
	int			x;
	int			y;
	t_square	*square;

	square->size = 0;
	x = 1;
	y = 1;
	while (x <= map->x)
	{
		while (y <= map->y)
		{
			map_n[x][y] = min(map_n[x - 1][y], map_n[x][y - 1], map_n[x - 1][y - 1]) + 1;
			y++;
			if (map_n[x][y] > square->size)
			{
				square->size = map_n[x][y];
				square->x = x - map_n[x][y];
				square->y = y - map_n[x][y];
			}
		}
		x++;
	}
	return (square);
}

char			**bsq(char **map, t_square *square, char full)
{
	int x;
	int y;

	x = square->x;
	y = square->y;
	while (x < square->size)
	{
		while (y < square->size)
		{
			map[x][y] = full;
			y++;
		}
		x++;
	}
	return(map);
}

void			print_bsq(char **map)
{
	int x;
	int y;
	while (map[x][0] != '\n')
	{
		while (map[x][y])
		{
			write(1, &map[x][y], 1);
			y++;
		}
		x++;
	}
}

int				min(int x, int y, int z)
{
	if (x < y && x < z)
		return (x);
	if (y < x && y < z)
		return (y);
	if (z < x && z < y)
		return (z);
	return(657567);
}