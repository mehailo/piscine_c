#ifndef BSQ_H
# define BSQ_H
# define SORRY (min(map_n[y - 1][x], map_n[y][x - 1], map_n[y - 1][x - 1]) + 1)

# include <unistd.h>
# include <stdlib.h>

typedef struct	s_sqare
{
	int			x;
	int			y;
	int			size;
}				t_square;

typedef	struct  s_map
{
	char		empty;
	char		obs;
	char		full;
	int			x;
	int			y;
}				t_map;

#endif