/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/19 13:55:38 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/19 13:56:13 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int				**do_numbers(char **map_s, t_map *map)
{
	int x;
	int y;
	int **map_n;

	map_n = (int **)malloc(sizeof(int*) * map->y);
	y = 0;
	while (y < map->y)
	{
		x = 0;
		map_n[y] = (int *)malloc(sizeof(int) * map->x);
		while (x < map->x)
		{
			if (map_s[y][x] == map->empty)
				map_n[y][x] = 1;
			if (map_s[y][x] == map->obs)
				map_n[y][x] = 0;
			x++;
		}
		y++;
	}
	return (map_n);
}

int				verif_elem(char **map_s, t_map *map)
{
	int x;
	int y;

	y = 0;
	while (y < map->y)
	{
		x = 0;
		while (x < map->x)
		{
			if (map_s[y][x] != map->empty && map_s[y][x] != map->obs)
				return (0);
			x++;
		}
		y++;
	}
	return (1);
}

int				verify_first(char *str)
{
	int i;

	i = 0;
	while (str[i + 3] != '\n')
	{
		if (str[i] < '0' && str[i] > '9')
			return (0);
		i++;
	}
	return (1);
}

t_map			*get_map_stuff(char *str)
{
	int		i;
	int		j;
	t_map	*map;

	map = (t_map *)malloc(sizeof(t_map));
	i = 0;
	j = 0;
	while (str[i] != '\n')
		i++;
	map->full = str[i - 1];
	map->obs = str[i - 2];
	map->empty = str[i - 3];
	map->y = atoi(str);
	i++;
	while (str[i + j] != '\n')
		j++;
	map->x = j;
	return (map);
}

int				verif_lines(char **map_s, t_map *map)
{
	int x;
	int y;
	int n;

	y = 0;
	while (y < map->y)
	{
		x = 0;
		n = 0;
		while (x < map->x && map_s[y][x])
		{
			n++;
			x++;
		}
		y++;
		if (n != map->x)
			return (0);
	}
	return (1);
}

int				verif_empty(char **map_s, t_map *map)
{
	int x;
	int y;

	y = 0;
	while (y < map->y)
	{
		x = 0;
		while (x < map->x)
		{
			if (map_s[y][x] == map->empty)
				return (1);
			x++;
		}
		y++;
	}
	return (0);
}

int				verif_enter(char **map_s, t_map *map)
{
	int y;

	y = 0;
	while (y < map->y)
	{
		if (map_s[y][map->x + 1] != '\n')
			return (0);
		y++;
	}
	return (1);
}

int				min(int x, int y, int z)
{
	if (x <= y && x <= z)
		return (x);
	if (y <= x && y <= z)
		return (y);
	if (z <= x && z <= y)
		return (z);
	return (x);
}

void			change_square(t_square *square, int val, int x, int y)
{
	square->size = val;
	square->x = x - val + 1;
	square->y = y - val + 1;
}

t_square		*do_good_numbers(int **map_n, t_map *map)
{
	int			x;
	int			y;
	t_square	*square;

	square = (t_square *)malloc(sizeof(t_square));
	square->size = 0;
	y = -1;
	while (++y < map->y)
	{
		x = -1;
		while (++x < map->x)
		{
			if (map_n[y][x])
			{
				if (x > 0 && y > 0)
					map_n[y][x] = SORRY;
				if (map_n[y][x] > square->size)
					change_square(square, map_n[y][x], x, y);
			}
		}
	}
	return (square);
}

char			*cut_the_str(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\n')
		i++;
	return (&str[i + 1]);
}

void			bsq(char **map_s, t_square *square, t_map *map)
{
	int x;
	int y;

	y = 0;
	while (y < map->y)
	{
		x = 0;
		while (x < map->x)
		{
			if ((y >= square->y && y <= (square->y + square->size - 1)) &&
				(x >= square->x && x <= (square->x + square->size - 1)))
				putchar(map->full);
			else
				putchar(map_s[y][x]);
			x++;
		}
		putchar('\n');
		y++;
	}
}

char			**do_the_array(char *str, t_map *map)
{
	int		x;
	int		y;
	char	**map_s;
	int		k;

	k = 0;
	x = 0;
	y = 0;
	while (str[x] != '\n')
		x++;
	map->x = x;
	map_s = (char **)malloc(sizeof(char *) * map->y);
	while (y < map->y)
	{
		x = 0;
		map_s[y] = (char *)malloc(sizeof(char) * map->x + 2);
		while (x < map->x)
			map_s[y][x++] = str[k++];
		k++;
		map_s[y][x + 1] = '\n';
		map_s[y++][x + 2] = '\0';
	}
	return (map_s);
}

char			*take_string(char *file)
{
	char	*s;
	int		fd;
	char	buff[100];
	int		i;
	int		res;

	i = 0;
	res = 0;
	fd = open(file, O_RDONLY);
	while ((res = read(fd, buff, 100)))
		i += res;
	close(fd);
	fd = open(file, O_RDONLY);
	s = (char*)malloc(i + 1);
	i = 0;
	while (read(fd, buff, 1))
		s[i++] = buff[0];
	close(fd);
	return (s);
}

char			*take_from_std(int fd)
{
	char	*s;
	char	buff[1];
	int		i;

	i = 0;
	s = (char *)malloc(10000);
	while (read(fd, buff, 1))
	{
		s[i++] = buff[0];
	}
	return (s);
}

void			do_job(char *str)
{
	char		**map_s;
	t_map		*map;
	t_square	*square;
	int			**num;

	map = get_map_stuff(str);
	map_s = do_the_array(cut_the_str(str), map);
	if (VERIF1 && VERIF2 && verify_first(str))
	{
		num = do_numbers(map_s, map);
		square = do_good_numbers(num, map);
		bsq(map_s, square, map);
	}
	else
	{
		write(1, "map error\n", 10);
	}
}

int				main(int ac, char **av)
{
	char*str;
	int i;
	int fd;

	if (ac == 1)
	{
		fd = 0;
		str = take_from_std(fd);
		do_job(str);
		free(str);
	}
	else
	{
		i = 1;
		while (i < ac)
		{
			str = take_string(av[i++]);
			do_job(str);
			free(str);
		}
	}
	return (0);
}
