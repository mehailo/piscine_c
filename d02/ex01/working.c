/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_reverse_alphabet.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 12:48:30 by mfrankev          #+#    #+#             */
/*   Updated: 2016/09/29 13:19:32 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_print_reverse_alphabet(void)
{
	char end;

	end = 'z';
	while (end >= 'a')
	{
		ft_putchar(end);
		end--;
	}
}

int		main(void)
{
	ft_print_reverse_alphabet();
	return (0);
}
