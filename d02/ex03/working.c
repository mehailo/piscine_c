/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_reverse_alphabet.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 12:48:30 by mfrankev          #+#    #+#             */
/*   Updated: 2016/09/29 16:52:00 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_is_negative(int n)
{
	if (n > 0)
	{
		ft_putchar('P');
	}
	if (n < 0)
	{
		ft_putchar('N');
	}
}

int		main(void)
{
	ft_is_negative(4);
	return (0);
}
