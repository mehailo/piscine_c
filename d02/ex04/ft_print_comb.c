/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 18:26:50 by mfrankev          #+#    #+#             */
/*   Updated: 2016/09/29 22:19:50 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

char g_out[3] = {'0', '1', '2'};

void	ft_print_comb(void)
{
	while (g_out[0] <= '7')
	{
		g_out[1] = g_out[0] + 1;
		while (g_out[1] <= '8')
		{
			g_out[2] = g_out[1] + 1;
			while (g_out[2] <= '9')
			{
				ft_putchar(g_out[0]);
				ft_putchar(g_out[1]);
				ft_putchar(g_out[2]);
				if (g_out[0] != '7' || g_out[1] != '8' || g_out[2] != '9')
				{
					ft_putchar(',');
					ft_putchar(' ');
				}
				g_out[2]++;
			}
			g_out[1]++;
		}
		g_out[0]++;
	}
}
