/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 21:46:25 by mfrankev          #+#    #+#             */
/*   Updated: 2016/09/29 22:06:41 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	ft_putnbr(int nb)
{
	int reverse;

	reverse = 0;
	while (nb != 0)
	{
		reverse = reverse * 10;
		reverse = reverse + nb % 10;
		nb = nb / 10;
	}
	while (reverse != 0)
	{
		ft_putchar(reverse % 10 + 48);
		reverse = reverse / 10;
	}
}
