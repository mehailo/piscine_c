/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 21:46:25 by mfrankev          #+#    #+#             */
/*   Updated: 2016/09/30 19:16:28 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_putchar(char c)
{
	write(1, &c, 1);
	return (0);
}

void	ft_putnbr(int nb)
{
	int reverse;

	reverse = 0;
	while (nb != 0)
	{
		reverse = reverse * 10;
		reverse = reverse + nb % 10;
		nb = nb / 10;
	}
	while (reverse != 0)
	{
		ft_putchar(reverse % 10 + 48);
		reverse = reverse / 10;
	}
}

int		main(void)
{
	ft_putnbr(-22214);
	return (0);
}
