/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/01 19:38:10 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/01 22:26:46 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_find_next_prime(int nb)
{
	int tmp;

	tmp = nb / 2 + 1;
	if (nb <= 2)
		return (2);
	while (tmp > 1)
	{
		if (nb % tmp == 0)
			return (ft_find_next_prime(nb + 1));
		tmp--;
	}
	return (nb);
}
