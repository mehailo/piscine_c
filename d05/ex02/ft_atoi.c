/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 12:07:10 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/04 21:03:25 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		atoi_garbage(char *str, int inx, int flag)
{
	if (str[inx] == '+' && flag)
	{
		inx++;
		flag = 0;
	}
	if (str[inx] == '+' && flag != 1)
		return (-1);
	if (str[inx] == '-' && flag != 1)
		return (-1);
	return (inx);
}

int		ft_atoi(char *str)
{
	int inx;
	int storage;
	int flag;

	storage = 0;
	inx = 0;
	flag = 1;
	while (str[inx] <= 32)
		inx++;
	if (str[inx] == '-')
	{
		inx++;
		flag = -1;
	}
	if (atoi_garbage(str, inx, flag) != -1)
		inx = atoi_garbage(str, inx, flag);
	while (str[inx] <= '9' && str[inx] >= '0')
	{
		storage *= 10;
		storage += (str[inx++] - '0');
	}
	if (flag >= 0)
		return (storage);
	return (-storage);
}
