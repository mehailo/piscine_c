/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 17:28:29 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/04 22:23:17 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcpy(char *dest, char *src)
{
	int index;

	index = 0;
	while (*src)
	{
		*dest = *src;
		dest++;
		src++;
		index++;
	}
	*dest = *src;
	return (dest - index);
}
