/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 17:28:29 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/03 20:49:26 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	int index;
	int dest_len;
	int src_len;

	index = 0;
	src_len = 0;
	dest_len = 0;
	while (src[src_len])
		src_len++;
	while (dest[dest_len])
		dest_len++;
	if (dest_len < src_len + 1)
		return (0);
	while (src[index] && n > 0)
	{
		dest[index] = src[index];
		index++;
		n--;
	}
	while (dest[index])
		dest[index] = '\0';
	return (dest);
}
