/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 17:28:29 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/03 20:49:26 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	int index;

	index = 0;
	while (*src && n > 0)
	{
		*dest = *src;
		dest++;
		src++;
		index++;
		n--;
	}
	*dest = *src;
	return (dest - index);
}
