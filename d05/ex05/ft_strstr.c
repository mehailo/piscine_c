/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 21:05:58 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/04 15:49:46 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_inn(char *str, char *tofind, int i, int j)
{
	int flag;
	int t_len;

	t_len = 0;
	while (tofind[t_len])
		t_len++;
	flag = 0;
	while (tofind[j])
	{
		if (str[i + j] == tofind[j])
			flag++;
		else
			flag = 0;
		if (flag == t_len)
			return (str + i);
		j++;
	}
	return (0);
}

char	*ft_strstr(char *str, char *tofind)
{
	char*res;
	int i;
	int j;

	i = 0;
	j = 0;
	res = 0;
	while (str[i] && res == 0)
	{
		j = 0;
		if (str[i] == tofind[j])
		{
			res = ft_inn(str, tofind, i, j);
		}
		i++;
	}
	return (res);
}
