/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 09:38:29 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/04 11:18:41 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>

char	*ft_strstr(char *str, char *tofind);

int		main(void)
{
	char src[] = "1234AA!!!56AAA78";
	char dest[] = "AAA";
	printf("%s", ft_strstr(src, dest));
}

