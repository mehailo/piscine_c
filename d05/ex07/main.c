/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 09:38:29 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/04 16:57:59 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>

int		ft_strncmp(char *s1, char *s2, unsigned int n);

int		main(void)
{
	char src[] = "12346AA!!!56AAA78";
	char dest[] = "12345AAA";
	printf("%d", ft_strncmp(src, dest, 2));
}

