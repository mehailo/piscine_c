/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 09:38:29 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/04 18:56:33 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>

int		ft_str_is_alpha(char *str);

int		main(void)
{
	char src[] = "AAAaaaabvfdbkvdfbsvJKHBK";
	printf("%d", ft_str_is_alpha(src));
}

