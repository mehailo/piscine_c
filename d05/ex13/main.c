/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 09:38:29 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/04 20:35:57 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>

int		ft_str_is_lowercase(char *str);

int		main(void)
{
	char src[] = "aaaabvfdbkvdfbsv";
	printf("%d", ft_str_is_lowercase(src));
}

