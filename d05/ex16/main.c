/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 09:38:29 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/04 22:50:51 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>

char	*ft_strcat(char *dest, char *str);

int		main(void)
{
	char src[] = "GGGGG";
	char dest[] = "BB435345BBB";
	printf("%s", ft_strcat(dest, src));
}

