/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/04 21:18:00 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/04 23:15:43 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncat(char *dest, char *src, int nb)
{
	int i;

	if (n <= 0)
		return (dest);
	i = 0;
	while (*dest)
	{
		dest++;
		i++;
	}
	while (*src && n)
	{
		*dest = *src;
		src++;
		dest++;
		i++;
		n--;
	}
	*dest = '\0';
	return (dest - i);
}
