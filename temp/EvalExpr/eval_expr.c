
#include "ft_stack.h"

void	do_op(t_list *queue, char oper)
{
	int first;
	int second;

	first = ft_pop_back_i(queue);
	second = ft_pop_back_i(queue);
	if (oper == '+')
		ft_push_back(queue, first + second);
	else if (oper == '-')
		ft_push_back(queue, first - second);
	else if (oper == '*')
		ft_push_back(queue, first * second);
	else if (oper == '/')
		ft_push_back(queue, first / second);
	else if (oper == '%')
		ft_push_back(queue, first % second);
}

int prior (char oper) 
{
	if (oper == '+' || oper == '-')
		return (1);
	if (oper == '*' || oper == '/' || oper == '%')
		return (2);
	return (0);
}

int		eval_expr(char *str)
{
	t_int_list *queue;
	t_list *stack;

	queue = NULL;
	stack = NULL;
	while (*str)
	{
		if (ft_is_oper(*str))
		{
			while(stack && prior(stack->data) >= prior(*str))
				do_op(queue, *str);
			ft_push_back(stack, *str);
		}
		else if (*str == '(')
			ft_push_back(stack, str)
		else if (*str == ')')
		{
			while (stack->data != '(')
			{
				do_op(queue, *str);
				ft_pop_back_c(stack);
			}
			ft_pop_back_c(stack);
		}
		else if (ft_is_num(*str))
			ft_push_back(queue, ft_atoi(get_num(&str)));
	}
	while(stack)
		do_op(queue, ft_pop_back_c(stack));
	return (stack->data);
}

/*
int		eval_expr(char *str)
{
	t_list *queue;
	t_list *stack;
	t_token token[ft_str_len_symbls(str)];
	t_token *begin;

	queue = NULL;
	stack = NULL;
	token = (t_token*)malloc(sizeof(t_token) * ft_str_len_symbls(str));
	while (*str)
	{
		if ((str == '-' && (ft_is_num(str))) || (ft_is_num(str)))
			(token++)->token = get_num(&str);
		else if (str == '(')
			ft_push(&stack, str++);
		else if (str == ')')
		{
			while(stack)
				(token++)->token = ft_pop(&stack);
		}
		else if (str == '+' || str == '-')
			ft_push(&stack, str++);
		else if (str == '*' || str == '/' || str == '%')
		{
			while(stack)
				(token++)->token = ft_pop(&stack);
			ft_push(&stack, str++);
		}
		else
			str++;
	}
	while(stack)
		(token++)->token = ft_pop(&stack);
}
*/

