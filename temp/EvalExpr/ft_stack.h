/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/15 17:41:57 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/15 17:52:04 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STACK_H
# define FT_STACK_H

# include <stdlib.h>
# include <unistd.h>

typedef struct		s_lst
{
	struct s_list	*next;
	int			    data;
}					t_int_list;

typedef struct		s_list
{
	struct s_list	*next;
	char			data;
}					t_list;

int 	ft_pop_back_i(t_int_list **begin_list);
void	ft_push(t_list **begin_list, void *data);
void	ft_push_back(t_int_list *list,void *data);
int		ft_is_num(char *str);
int		ft_is_oper(char str);
char	get_num(char **str);
int		atoi_garbage(char *str, int inx, int flag);
int		ft_atoi(char *str);

#endif