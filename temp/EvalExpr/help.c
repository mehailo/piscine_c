int		ft_is_num(char *str)
{
	if (*str >= '0' && *str <= '9')
		return (1);
	if (*str == '-' && *(ft_is_num(str + 1)))
		return (1);
	return (0);
}

int		ft_is_oper(char str)
{
	if (str == '+' || str == '-')
		return (1);
	if (str == '*' || str == '/' || str == '%')
		return (1);	
	return (0);
}

char	get_num(char **str)
{
	int i;
	int j;
	char *out;


	i = 0;
	j = 0;
	while (str[i] > 32)
		i++;
	b = (char*)malloc(sizeof(char) * 1);
	while (j < i)
	{
		*out = *str;
		out++;
		str++;
		j++
	}
	*str = *str - i;
	return (out - i);
}

int		atoi_garbage(char *str, int inx, int flag)
{
	if (str[inx] == '+' && flag)
	{
		inx++;
		flag = 0;
	}
	if (str[inx] == '+' && flag != 1)
		return (-1);
	if (str[inx] == '-' && flag != 1)
		return (-1);
	return (inx);
}

int		ft_atoi(char *str)
{
	int inx;
	int storage;
	int flag;

	storage = 0;
	inx = 0;
	flag = 1;
	while (str[inx] <= 32)
		inx++;
	if (str[inx] == '-')
	{
		inx++;
		flag = -1;
	}
	if (atoi_garbage(str, inx, flag) != -1)
		inx = atoi_garbage(str, inx, flag);
	while (str[inx] <= '9' && str[inx] >= '0')
	{
		storage *= 10;
		storage += (str[inx++] - '0');
	}
	if (flag >= 0)
		return (storage);
	return (-storage);
}