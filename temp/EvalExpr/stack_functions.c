/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_functions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mfrankev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/15 17:52:32 by mfrankev          #+#    #+#             */
/*   Updated: 2016/10/15 18:10:31 by mfrankev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stack.h"

t_list	*ft_create_elem(void *data)
{
	t_list *list;

	list = (t_list*)malloc(sizeof(t_list));
	if (list)
	{
		list->data = data;
		list->next = NULL;
	}
	return (list);
}

void	ft_push(t_list **begin_list, void *data)
{
	t_list *tmp;

	tmp = ft_create_elem(data);
	tmp->next = *begin_list;
	*begin_list = tmp;
}

int 	ft_pop_back_i(t_int_list **begin_list)
{
	t_int_list *tmp;

	temp = *begin_list;
	if (temp)
		*begin_list = tmp->next;
	return tmp->data;
}

char 	ft_pop_back_c(t_list **begin_list)
{
	t_list *tmp;

	temp = *begin_list;
	if (temp)
		*begin_list = tmp->next;
	return tmp->data;
}

void	ft_push_back(t_int_list *list,void *data)
{
	t_int_list *tmp;
	t_int_list *cur;

	cur = *list;
	tmp = ft_create_elem(data);
	if (*list == NULL)
		*list = tmp;
	else
	{
		while (cur->next)
 			cur = cur->next;
		cur->next = tmp;
	}
}

void	ft_push_back(t_list **begin_list, void *data)
{
	t_list *tmp;
	t_list *cur;

	cur = *begin_list;
	tmp = ft_create_elem(data);
	if (*begin_list == NULL)
		*begin_list = tmp;
	else
	{
		while (cur->next)
 			cur = cur->next;
		cur->next = tmp;
	}
}
