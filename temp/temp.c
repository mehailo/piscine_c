

#include <unistd.h>

typedef int (*funptr)(char);

int 	ft_putchar(char c)
{
	write (1, &c, 1);
	return (0);
}

void	ft_putstr(char *str)
{
	int index;

	index = 0;
	while (str[index])
	{
		ft_putchar(str[index++]);
	}
}

int 	main(void)
{
	funptr	f;
	f = &ft_putchar;
	char a[] = "hey!";
	f('a');
	return (0);
}

