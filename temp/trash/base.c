#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int g_b = 0;

int		ft_base(char *str, int base)
{
	int len;
	int out;
	int flag[1];

	if (base > 10)
		g_b = 10;
	else
		g_b = base;
	len = 0;
	flag[0] = 0;
	if (str[len] == '-')
		flag[len++] = -1;
	printf("~~~string - %s base = %d g_d = %d\n", str, base, g_b);
	while (((str[len] >= '0' && str[len] <= ('0' + g_b - 1)) ||
		(str[len] >= 'a' && str[len] <= ('a' + base - 11))) && str[len])
	{
		if ((str[len] >= '0' && str[len] <= ('0' + g_b - 1)))
			printf("first is ok on symb %c\n", str[len]);
		if ((str[len] >= 'a' && str[len] <= ('a' + base - 11)))
			printf("second is ok\n");
		len++;
		if ((str[len] >= '0' && str[len] <= ('0' + g_b - 1)))
			printf("first++ is ok on symb %c\n", str[len]);
		if ((str[len] >= 'a' && str[len] <= ('a' + base - 11)))
			printf("second++ is ok\n");
	}
	len--;
	printf("~~~string - %s len = %d\n", str, len);
	while (len > 0 && str[len] != '-' && str[len] != '0')
	{
		out = out * base;
		if (str[len] >= '0' && str[len] <= '9')
			out += str[len--] - '0';
		else if (str[len] >= 'a' && str[len] <= 'f')
			out += str[len--] - 'a' + 10;
		else
			len--;
		printf("im on %d symbols, the symbol is %c current val is %d\n", len, str[len + 1], out);
	}
	if (flag[0])
		return (-out);
	return (out);
}

int		main(int argc, char **argv)
{
	int base;

	if (argc == 1)
		printf("need 2 arguments\n");
	base = atoi(argv[2]);
	printf("%d\n", ft_base(argv[1], base));
	return (0);
}