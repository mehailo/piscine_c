#include <unistd.h>
#include <stdlib.h>

char *g_letters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

int		main(int argc, char **argv)
{
	int nb;
	char out[255];
	int i;
	int base;

	base = 16;
	if (argc == 3)
		base = atoi(argv[2]);
	i = 0;
	if (argc == 1)
		return (0);
	nb = atoi(argv[1]);
	while (nb)
	{
		out[i++] = g_letters[nb % base];
		nb = nb / base;
	}
	i--;
	while (i >= 0)
		write(1, &out[i--], 1);
	return (0);
}